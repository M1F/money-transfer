package org.bitbucket.mif.money.transfer.server.api;

import org.bitbucket.mif.money.transfer.api.rest.RestApi;

import com.sun.net.httpserver.HttpExchange;

public class JavaHttpRestApi implements RestApi<HttpExchange> {

	private final RestApi<HttpExchange> delegate;

	JavaHttpRestApi(RestApi<HttpExchange> delegate) {
		this.delegate = delegate;
	}

	@Override
	public String getPath() {
		return delegate.getPath();
	}

	@Override
	public String getMethod() {
		return delegate.getMethod();
	}

	@Override
	public String process(HttpExchange request) {
		return delegate.process(request);
	}

	public boolean shouldBeProcessed(HttpExchange request) {
		return getMethod().equals(request.getRequestMethod());
	}
}
