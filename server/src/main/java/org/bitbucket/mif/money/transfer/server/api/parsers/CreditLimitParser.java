package org.bitbucket.mif.money.transfer.server.api.parsers;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.function.Function;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;

public class CreditLimitParser implements Function<HttpExchange, BigDecimal> {
	private final ObjectMapper objectMapper;

	public CreditLimitParser(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	@Override
	public BigDecimal apply(HttpExchange httpExchange) {
		try {
			return objectMapper.readValue(httpExchange.getRequestBody(), CreditLimit.class).newValue;
		}
		catch (IOException e) {
			throw new RuntimeException("Can't read new credit limit value from request body", e);
		}
	}

	private static class CreditLimit {
		private BigDecimal newValue;

		void setNewValue(BigDecimal newValue) {
			this.newValue = newValue;
		}
	}
}
