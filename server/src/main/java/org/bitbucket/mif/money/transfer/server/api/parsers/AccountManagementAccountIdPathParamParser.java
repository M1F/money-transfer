package org.bitbucket.mif.money.transfer.server.api.parsers;

public class AccountManagementAccountIdPathParamParser extends AccountIdPathParamParser {

	@Override
	String partOfPathBeforeId() {
		return "/account/";
	}
}
