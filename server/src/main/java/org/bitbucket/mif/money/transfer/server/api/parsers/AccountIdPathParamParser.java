package org.bitbucket.mif.money.transfer.server.api.parsers;

import java.util.function.Function;

import com.sun.net.httpserver.HttpExchange;

public abstract class AccountIdPathParamParser implements Function<HttpExchange, String> {

	@Override
	public String apply(HttpExchange httpExchange) {
		String path = httpExchange.getRequestURI().getRawPath();
		int indexBeforeAccountId = path.lastIndexOf(partOfPathBeforeId()) + partOfPathBeforeId().length();
		String[] accountIdWithEndPart = path.substring(indexBeforeAccountId).split("/");
		return accountIdWithEndPart[0];
	}

	abstract String partOfPathBeforeId();
}
