package org.bitbucket.mif.money.transfer.server.api.impl;

import java.math.BigDecimal;

import org.bitbucket.mif.money.transfer.api.AccountManagementResource;
import org.bitbucket.mif.money.transfer.core.account.management.AccountManagement;
import org.bitbucket.mif.money.transfer.core.domain.AccountId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccountManagementService implements AccountManagementResource {
	private final static Logger LOGGER = LoggerFactory.getLogger(AccountManagementService.class);
	private final AccountManagement accountManagement;

	public AccountManagementService(AccountManagement accountManagement) {
		this.accountManagement = accountManagement;
	}

	@Override
	public void createAccount(String accountId) {
		LOGGER.info("start creation account with id {}", accountId);
		accountManagement.createAccountWith(new AccountId(accountId));
		LOGGER.info("Created account with id {}", accountId);

	}

	@Override
	public void setCreditLimit(String accountId, BigDecimal creditLimit) {
		LOGGER.info("start updating credit limit {} for {} account", creditLimit, accountId);
		accountManagement.updateCreditLimitFor(new AccountId(accountId), creditLimit);
		LOGGER.info("Credit limit for {} account was updated to {}", accountId, creditLimit);
	}

	@Override
	public void deleteAccount(String accountId) {
		LOGGER.info("start deleting account with id {}", accountId);
		accountManagement.deleteAccountWith(new AccountId(accountId));
		LOGGER.info("Account with id {} was deleted", accountId);
	}

	@Override
	public BigDecimal getBalance(String accountId) {
		LOGGER.info("start retrieve balance for account with id {}", accountId);
		BigDecimal balance = accountManagement.getBalanceOf(new AccountId(accountId));
		LOGGER.info("Balance for account with id {} is {}", accountId, balance);
		return balance;
	}
}
