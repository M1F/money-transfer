package org.bitbucket.mif.money.transfer.server;

public interface HttpServer {
	void start();

	void stop();
}
