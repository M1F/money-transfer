package org.bitbucket.mif.money.transfer.server.api.parsers;

import java.io.IOException;
import java.util.function.Function;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;

public class AccountIdBodyParser implements Function<HttpExchange, String> {
	private final ObjectMapper objectMapper;

	public AccountIdBodyParser(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	@Override
	public String apply(HttpExchange httpExchange) {
		try {
			return objectMapper.readValue(httpExchange.getRequestBody(), AccountId.class).accountId;
		}
		catch (IOException e) {
			throw new RuntimeException("Can't read account id from request body", e);
		}
	}

	private static class AccountId {
		private String accountId;

		void setAccountId(String accountId) {
			this.accountId = accountId;
		}
	}
}
