package org.bitbucket.mif.money.transfer.server.api.parsers;

public class DepositAccountIdPathParamParser extends AccountIdPathParamParser {
	@Override
	String partOfPathBeforeId() {
		return "/deposit/";
	}
}
