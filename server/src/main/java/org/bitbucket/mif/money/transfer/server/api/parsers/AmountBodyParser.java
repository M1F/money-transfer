package org.bitbucket.mif.money.transfer.server.api.parsers;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.function.Function;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;

public class AmountBodyParser implements Function<HttpExchange, BigDecimal> {
	private final ObjectMapper objectMapper;

	public AmountBodyParser(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	@Override
	public BigDecimal apply(HttpExchange httpExchange) {
		try {
			return objectMapper.readValue(httpExchange.getRequestBody(), Amount.class).amount;
		}
		catch (IOException e) {
			throw new RuntimeException("Can't read amount value from request body", e);
		}
	}

	private static class Amount {
		private BigDecimal amount;

		void setAmount(BigDecimal amount) {
			this.amount = amount;
		}
	}
}
