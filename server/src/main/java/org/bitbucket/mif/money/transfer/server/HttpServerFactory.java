package org.bitbucket.mif.money.transfer.server;

import java.io.IOException;

import org.bitbucket.mif.money.transfer.server.api.RestApiFactory;

public interface HttpServerFactory {

	static HttpServer create(int port) throws IOException {
		MoneyTransferHttpServer server = new MoneyTransferHttpServer(port);
		server.init(RestApiFactory.create());
		return server;
	}
}
