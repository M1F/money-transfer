package org.bitbucket.mif.money.transfer.server;

import static java.util.stream.Collectors.groupingBy;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.bitbucket.mif.money.transfer.api.rest.RestApi;
import org.bitbucket.mif.money.transfer.server.api.JavaHttpRestApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

class MoneyTransferHttpServer implements org.bitbucket.mif.money.transfer.server.HttpServer {
	private static final Logger LOGGER = LoggerFactory.getLogger(MoneyTransferHttpServer.class);
	private final HttpServer server;

	MoneyTransferHttpServer(int serverPort) throws IOException {
		server = HttpServer.create(new InetSocketAddress(serverPort), 0);
	}

	void init(List<JavaHttpRestApi> supportedApis) {

		Map<String, List<JavaHttpRestApi>> groupedByPath = supportedApis.stream().collect(groupingBy(RestApi::getPath));

		groupedByPath.forEach((path, restApis) -> server.createContext(path, createRequestsHandler(restApis)));

		Executor executor = Executors.newCachedThreadPool();
		server.setExecutor(executor);
	}

	private HttpHandler createRequestsHandler(List<JavaHttpRestApi> restApis) {
		return exchange -> {
			boolean wasProcessed = false;
			for (JavaHttpRestApi restApi : restApis) {
				if (restApi.shouldBeProcessed(exchange)) {
					process(exchange, restApi);
					wasProcessed = true;
					break;
				}
			}
			if (!wasProcessed) {
				sendErrorResponseSafely(exchange, 405);// 405 Method Not Allowed
			}
			exchange.close();
		};
	}

	private void process(HttpExchange exchange, JavaHttpRestApi restApi) {
		try {
			String respText = restApi.process(exchange);
			sendGoodResponseSafely(exchange, respText);
		}
		catch (Throwable e) {
			LOGGER.error("Error during processing request", e);
			sendErrorResponseSafely(exchange, 400);
		}
	}

	@Override
	public void start() {
		server.start();
	}

	@Override
	public void stop() {
		server.stop(1);
	}

	private void sendGoodResponseSafely(HttpExchange exchange, String respText) {
		try {
			exchange.sendResponseHeaders(200, respText.getBytes().length);
			OutputStream output = exchange.getResponseBody();
			output.write(respText.getBytes());
			output.flush();
		}
		catch (IOException e) {
			LOGGER.error("Could not send error response", e);
		}
	}

	private void sendErrorResponseSafely(HttpExchange exchange, int errorCode) {
		try {
			exchange.sendResponseHeaders(errorCode, -1);
		}
		catch (IOException e) {
			LOGGER.error("Could not send error response", e);
		}
	}
}
