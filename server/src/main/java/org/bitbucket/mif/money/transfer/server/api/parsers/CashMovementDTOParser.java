package org.bitbucket.mif.money.transfer.server.api.parsers;

import java.io.IOException;
import java.util.function.Function;

import org.bitbucket.mif.money.transfer.api.dto.CashMovementDTO;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;

public class CashMovementDTOParser implements Function<HttpExchange, CashMovementDTO> {
	private final ObjectMapper objectMapper;

	public CashMovementDTOParser(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	@Override
	public CashMovementDTO apply(HttpExchange httpExchange) {
		try {
			return objectMapper.readValue(httpExchange.getRequestBody(), CashMovementDTO.class);
		}
		catch (IOException e) {
			throw new RuntimeException("Can't read cash movement information from request body", e);
		}
	}
}
