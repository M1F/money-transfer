package org.bitbucket.mif.money.transfer.server.api;

import java.util.Arrays;
import java.util.List;

import org.bitbucket.mif.money.transfer.api.AccountManagementResource;
import org.bitbucket.mif.money.transfer.api.CashMovementResource;
import org.bitbucket.mif.money.transfer.api.rest.RestApi;
import org.bitbucket.mif.money.transfer.api.rest.account.AccountBalanceApi;
import org.bitbucket.mif.money.transfer.api.rest.account.AccountCreationApi;
import org.bitbucket.mif.money.transfer.api.rest.account.AccountCreditLimitUpdateApi;
import org.bitbucket.mif.money.transfer.api.rest.account.AccountDeletionApi;
import org.bitbucket.mif.money.transfer.api.rest.cash.movement.CashMovementDepositApi;
import org.bitbucket.mif.money.transfer.api.rest.cash.movement.CashMovementTransferApi;
import org.bitbucket.mif.money.transfer.core.account.management.AccountManagement;
import org.bitbucket.mif.money.transfer.core.cash.movement.CashMovement;
import org.bitbucket.mif.money.transfer.server.api.impl.AccountManagementService;
import org.bitbucket.mif.money.transfer.server.api.impl.CashMovementService;
import org.bitbucket.mif.money.transfer.server.api.parsers.AccountIdBodyParser;
import org.bitbucket.mif.money.transfer.server.api.parsers.AccountManagementAccountIdPathParamParser;
import org.bitbucket.mif.money.transfer.server.api.parsers.AmountBodyParser;
import org.bitbucket.mif.money.transfer.server.api.parsers.CashMovementDTOParser;
import org.bitbucket.mif.money.transfer.server.api.parsers.CreditLimitParser;
import org.bitbucket.mif.money.transfer.server.api.parsers.DepositAccountIdPathParamParser;
import org.bitbucket.mif.money.transfer.storage.ClientStorage;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;

public interface RestApiFactory {

	static List<JavaHttpRestApi> create() {
		ObjectMapper objectMapper = new ObjectMapper();

		ClientStorage clientStorage = new ClientStorage();

		AccountManagementResource accountManagementResource = new AccountManagementService(new AccountManagement(clientStorage, clientStorage));
		CashMovementResource cashMovementResource = new CashMovementService(new CashMovement(clientStorage));

		AccountManagementAccountIdPathParamParser accountManagementAccountIdPathParamParser = new AccountManagementAccountIdPathParamParser();
		return Arrays.asList(
				wrap(new AccountCreationApi<>(accountManagementResource, new AccountIdBodyParser(objectMapper))),
				wrap(new AccountCreditLimitUpdateApi<>(accountManagementResource, accountManagementAccountIdPathParamParser,
						new CreditLimitParser(objectMapper))),
				wrap(new AccountDeletionApi<>(accountManagementResource, accountManagementAccountIdPathParamParser)),
				wrap(new AccountBalanceApi<>(accountManagementResource, accountManagementAccountIdPathParamParser)),
				wrap(new CashMovementDepositApi<>(cashMovementResource, new DepositAccountIdPathParamParser(),
						new AmountBodyParser(objectMapper))),
				wrap(new CashMovementTransferApi<>(cashMovementResource, new CashMovementDTOParser(objectMapper))));
	}

	private static JavaHttpRestApi wrap(RestApi<HttpExchange> delegate) {
		return new JavaHttpRestApi(delegate);
	}
}
