package org.bitbucket.mif.money.transfer.server.api.impl;

import java.math.BigDecimal;

import org.bitbucket.mif.money.transfer.api.CashMovementResource;
import org.bitbucket.mif.money.transfer.api.dto.CashMovementDTO;
import org.bitbucket.mif.money.transfer.core.cash.movement.CashMovement;
import org.bitbucket.mif.money.transfer.core.domain.AccountId;
import org.bitbucket.mif.money.transfer.core.domain.CashTransfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CashMovementService implements CashMovementResource {
	private final static Logger LOGGER = LoggerFactory.getLogger(CashMovementService.class);
	private final CashMovement cashMovement;

	public CashMovementService(CashMovement cashMovement) {
		this.cashMovement = cashMovement;
	}

	@Override
	public void deposit(String account, BigDecimal depositAmount) {
		LOGGER.info("start make deposit for {} with amount {}", account, depositAmount);
		cashMovement.depositFor(new AccountId(account), depositAmount);
		LOGGER.info("deposit for {} with amount {} was done", account, depositAmount);

	}

	@Override
	public void transfer(CashMovementDTO cashMovementDTO) {
		LOGGER.info("start transferOf money for {}", cashMovementDTO);
		cashMovement.transferOf(convert(cashMovementDTO));
	}

	private CashTransfer convert(CashMovementDTO cashMovementDTO) {
		return new CashTransfer(new AccountId(cashMovementDTO.getAccountFrom()), new AccountId(cashMovementDTO.getAccountTo()),
				cashMovementDTO.getAmount());
	}
}
