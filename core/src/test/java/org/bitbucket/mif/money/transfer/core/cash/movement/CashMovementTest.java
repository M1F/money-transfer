package org.bitbucket.mif.money.transfer.core.cash.movement;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.BigDecimalCloseTo.closeTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.bitbucket.mif.money.transfer.core.domain.AccountId;
import org.bitbucket.mif.money.transfer.core.domain.CashTransfer;
import org.bitbucket.mif.money.transfer.core.domain.Client;
import org.bitbucket.mif.money.transfer.core.domain.exceptions.NotEnoughMoneyException;
import org.junit.Test;

public class CashMovementTest {

	@Test
	public void depositFor_delegatedToClient() {
		//GIVEN
		AccountId accountId = new AccountId("acc1");
		BigDecimal depositAmount = BigDecimal.TEN;

		Client mockClient = mock(Client.class);
		ClientProvider clientProvider = mock(ClientProvider.class);
		when(clientProvider.retrieveClientBy(accountId)).thenReturn(mockClient);

		CashMovement cashMovement = new CashMovement(clientProvider);
		//WHEN
		cashMovement.depositFor(accountId, depositAmount);
		//THEN
		verify(mockClient).deposit(depositAmount);
	}

	@Test
	public void transferOf_doWithdrawalAndDeposit() {
		//GIVEN
		AccountId accountFrom = new AccountId("acc1");
		AccountId accountTo = new AccountId("acc2");
		BigDecimal transferAmount = BigDecimal.TEN;

		Client clientFrom = mock(Client.class);
		Client clientTo = mock(Client.class);
		ClientProvider clientProvider = mock(ClientProvider.class);
		when(clientProvider.retrieveClientBy(accountFrom)).thenReturn(clientFrom);
		when(clientProvider.retrieveClientBy(accountTo)).thenReturn(clientTo);

		CashMovement cashMovement = new CashMovement(clientProvider);
		//WHEN
		cashMovement.transferOf(new CashTransfer(accountFrom, accountTo, transferAmount));
		//THEN
		verify(clientFrom).withdrawal(transferAmount);
		verifyNoMoreInteractions(clientFrom);
		verify(clientTo).deposit(transferAmount);
		verifyNoMoreInteractions(clientTo);
	}

	@Test(expected = NotEnoughMoneyException.class)
	public void transferOf_doWithdrawalAndDeposit_notEnoughMoney() {
		//GIVEN
		AccountId accountFrom = new AccountId("acc1");
		AccountId accountTo = new AccountId("acc2");
		BigDecimal transferAmount = BigDecimal.TEN;

		Client clientFrom = new Client(accountFrom);
		Client clientTo = mock(Client.class);
		ClientProvider clientProvider = mock(ClientProvider.class);
		when(clientProvider.retrieveClientBy(accountFrom)).thenReturn(clientFrom);
		when(clientProvider.retrieveClientBy(accountTo)).thenReturn(clientTo);

		CashMovement cashMovement = new CashMovement(clientProvider);
		//WHEN
		NotEnoughMoneyException result = null;
		try {
			cashMovement.transferOf(new CashTransfer(accountFrom, accountTo, transferAmount));
		}
		catch (NotEnoughMoneyException e) {
			result = e;
		}

		//THEN
		assertThat(clientFrom.balance(), closeTo(BigDecimal.ZERO, BigDecimal.ZERO));
		verify(clientTo, never()).deposit(transferAmount);
		verifyNoMoreInteractions(clientTo);

		throw result;
	}
}
