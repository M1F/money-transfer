package org.bitbucket.mif.money.transfer.core.domain;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.BigDecimalCloseTo.closeTo;

import java.math.BigDecimal;

import org.bitbucket.mif.money.transfer.core.domain.exceptions.NotEnoughMoneyException;
import org.junit.Test;

public class ClientTest {
	private static final BigDecimal POSSIBLE_DELTA = BigDecimal.ZERO;

	@Test
	public void whenWeDo_deposit_balanceIncreased() {
		//GIVEN
		AccountId accountId = new AccountId("1");
		Client client = new Client(accountId);
		BigDecimal depositAmount = BigDecimal.valueOf(100);
		//WHEN
		client.deposit(depositAmount);
		//THEN
		assertThat(client.balance(), closeTo(depositAmount, POSSIBLE_DELTA));
	}

	@Test(expected = NotEnoughMoneyException.class)
	public void whenWeDo_withdrawal_withZeroBalanceAndCreditLimit_weGetException() {
		//GIVEN
		AccountId accountId = new AccountId("1");
		Client client = new Client(accountId);
		BigDecimal withdrawalAmount = BigDecimal.valueOf(100);
		//WHEN
		client.withdrawal(withdrawalAmount);
		//THEN
		assertThat(client.balance(), closeTo(BigDecimal.ZERO, POSSIBLE_DELTA));
	}

	@Test
	public void whenWeDo_withdrawal_withBalanceGtWithdrawal_balanceBecomeSmaller() {
		//GIVEN
		AccountId accountId = new AccountId("1");
		Client client = new Client(accountId);
		BigDecimal depositAmount = BigDecimal.valueOf(150);
		BigDecimal withdrawalAmount = BigDecimal.valueOf(100);
		//WHEN
		client.deposit(depositAmount);
		client.withdrawal(withdrawalAmount);
		//THEN
		assertThat(client.balance(), closeTo(BigDecimal.valueOf(50), POSSIBLE_DELTA));
	}

	@Test
	public void whenWeDo_withdrawal_withBalancePlusCreditLimitGTWithdrawal_balanceIsNegative() {
		//GIVEN
		AccountId accountId = new AccountId("1");
		Client client = new Client(accountId);
		BigDecimal depositAmount = BigDecimal.valueOf(100);
		BigDecimal withdrawalAmount = BigDecimal.valueOf(150);
		BigDecimal creditLimit = BigDecimal.valueOf(100);
		//WHEN
		client.updateCreditLimit(creditLimit);
		client.deposit(depositAmount);
		client.withdrawal(withdrawalAmount);
		//THEN
		assertThat(client.balance(), closeTo(BigDecimal.valueOf(-50), POSSIBLE_DELTA));
	}

	@Test
	public void whenWeDo_withdrawal_withZeroBalanceAndCreditLimitEqWithdrawal_balanceIsNegative() {
		//GIVEN
		AccountId accountId = new AccountId("1");
		Client client = new Client(accountId);
		BigDecimal withdrawalAmount = BigDecimal.valueOf(150);
		BigDecimal creditLimit = BigDecimal.valueOf(150);
		//WHEN
		client.updateCreditLimit(creditLimit);
		client.withdrawal(withdrawalAmount);
		//THEN
		assertThat(client.balance(), closeTo(BigDecimal.valueOf(-150), POSSIBLE_DELTA));
	}
}
