package org.bitbucket.mif.money.transfer.core.domain;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsSame.sameInstance;

import java.math.BigDecimal;

import org.junit.Test;

public class ClientInfoTest {

	@Test
	public void whenWe_updateCreditLimit_weReceiveNewRef() {
		//GIVEN
		ClientInfo clientInfo = new ClientInfo();
		//WHEN
		ClientInfo result = clientInfo.updateCreditLimit(BigDecimal.TEN);
		//THEN
		assertThat(result, not(sameInstance(clientInfo)));
	}

	@Test
	public void whenWeCall_isAboveLimit_withValueGtLimit_weGetTrue() {
		//GIVEN
		ClientInfo clientInfo = new ClientInfo();
		//WHEN
		boolean result = clientInfo.isAboveLimit(BigDecimal.TEN);
		//THEN
		assertThat(result, is(true));
	}

	@Test
	public void whenWeCall_isAboveLimit_withValueEqLimit_weGetFalse() {
		//GIVEN
		ClientInfo clientInfo = new ClientInfo();
		//WHEN
		clientInfo = clientInfo.updateCreditLimit(BigDecimal.TEN);
		boolean result = clientInfo.isAboveLimit(BigDecimal.TEN);
		//THEN
		assertThat(result, is(false));
	}

	@Test
	public void whenWeCall_isAboveLimit_withValueLtLimit_weGetFalse() {
		//GIVEN
		ClientInfo clientInfo = new ClientInfo();
		//WHEN
		clientInfo = clientInfo.updateCreditLimit(BigDecimal.TEN);
		boolean result = clientInfo.isAboveLimit(BigDecimal.ONE);
		//THEN
		assertThat(result, is(false));
	}
}
