package org.bitbucket.mif.money.transfer.core.domain;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.BigDecimalCloseTo.closeTo;

import java.math.BigDecimal;

import org.junit.Test;

public class ClientBalanceTest {
	private static final BigDecimal POSSIBLE_DELTA = BigDecimal.ZERO;

	@Test(expected = IllegalArgumentException.class)
	public void whenWeAdd_negativeValue_weGetException() {
		//GIVEN
		ClientBalance clientBalance = new ClientBalance();
		//WHEN
		clientBalance.add(BigDecimal.valueOf(-1));
		//THEN
		throw new RuntimeException("should be thrown another exception");
	}

	@Test
	public void whenWeAdd_positiveValue_balanceIncreased() {
		//GIVEN
		ClientBalance clientBalance = new ClientBalance();
		//WHEN
		clientBalance.add(BigDecimal.ONE);
		//THEN
		assertThat(clientBalance.get(), closeTo(BigDecimal.ONE, POSSIBLE_DELTA));
	}

	@Test
	public void whenWeSubtract_positiveValue_balanceIsSmaller() {
		//GIVEN
		ClientBalance clientBalance = new ClientBalance();
		//WHEN
		clientBalance.subtract(BigDecimal.ONE);
		//THEN
		assertThat(clientBalance.get(), closeTo(BigDecimal.valueOf(-1), POSSIBLE_DELTA));
	}

	@Test
	public void whenWeSubtract_negativeValue_balanceIsSmaller() {
		//GIVEN
		ClientBalance clientBalance = new ClientBalance();
		//WHEN
		clientBalance.subtract(BigDecimal.valueOf(-10));
		//THEN
		assertThat(clientBalance.get(), closeTo(BigDecimal.valueOf(-10), POSSIBLE_DELTA));
	}

	@Test
	public void whenWeAddAndSubtract_balanceIsChanged() {
		//GIVEN
		ClientBalance clientBalance = new ClientBalance();
		//WHEN
		clientBalance.add(BigDecimal.valueOf(60));
		clientBalance.subtract(BigDecimal.valueOf(-10));
		//THEN
		assertThat(clientBalance.get(), closeTo(BigDecimal.valueOf(50), POSSIBLE_DELTA));
	}
}
