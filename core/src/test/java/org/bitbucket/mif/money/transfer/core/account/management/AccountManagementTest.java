package org.bitbucket.mif.money.transfer.core.account.management;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.math.BigDecimal;

import org.bitbucket.mif.money.transfer.core.account.BalanceProvider;
import org.bitbucket.mif.money.transfer.core.domain.AccountId;
import org.junit.Test;

public class AccountManagementTest {
	@Test
	public void createAccountWith_delegatedToStorage() {
		//GIVEN
		AccountInfoStorage accountInfoStorage = mock(AccountInfoStorage.class);
		BalanceProvider balanceProvider = mock(BalanceProvider.class);
		AccountManagement accountManagement = new AccountManagement(accountInfoStorage, balanceProvider);
		AccountId accountId = new AccountId("acc1");
		//WHEN
		accountManagement.createAccountWith(accountId);
		//THEN
		verify(accountInfoStorage).createAccountWith(accountId);
		verifyNoMoreInteractions(accountInfoStorage);
		verifyNoMoreInteractions(balanceProvider);
	}

	@Test
	public void deleteAccountWith_delegatedToStorage() {
		//GIVEN
		AccountInfoStorage accountInfoStorage = mock(AccountInfoStorage.class);
		BalanceProvider balanceProvider = mock(BalanceProvider.class);
		AccountManagement accountManagement = new AccountManagement(accountInfoStorage, balanceProvider);
		AccountId accountId = new AccountId("acc1");
		//WHEN
		accountManagement.deleteAccountWith(accountId);
		//THEN
		verify(accountInfoStorage).deleteAccountWith(accountId);
		verifyNoMoreInteractions(accountInfoStorage);
		verifyNoMoreInteractions(balanceProvider);
	}

	@Test
	public void updateCreditLimitFor_delegatedToStorage() {
		//GIVEN
		AccountInfoStorage accountInfoStorage = mock(AccountInfoStorage.class);
		BalanceProvider balanceProvider = mock(BalanceProvider.class);
		AccountManagement accountManagement = new AccountManagement(accountInfoStorage, balanceProvider);
		AccountId accountId = new AccountId("acc1");
		BigDecimal newCreditLimit = BigDecimal.TEN;
		//WHEN
		accountManagement.updateCreditLimitFor(accountId, newCreditLimit);
		//THEN
		verify(accountInfoStorage).updateCreditLimitFor(accountId, newCreditLimit);
		verifyNoMoreInteractions(accountInfoStorage);
		verifyNoMoreInteractions(balanceProvider);
	}

	@Test
	public void getBalanceOf_delegatedToBalanceProvider() {
		//GIVEN
		AccountInfoStorage accountInfoStorage = mock(AccountInfoStorage.class);
		BalanceProvider balanceProvider = mock(BalanceProvider.class);
		AccountManagement accountManagement = new AccountManagement(accountInfoStorage, balanceProvider);
		AccountId accountId = new AccountId("acc1");
		//WHEN
		accountManagement.getBalanceOf(accountId);
		//THEN
		verify(balanceProvider).getBalanceOf(accountId);
		verifyNoMoreInteractions(balanceProvider);
		verifyNoMoreInteractions(accountInfoStorage);
	}
}
