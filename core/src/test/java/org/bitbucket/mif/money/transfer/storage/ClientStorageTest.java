package org.bitbucket.mif.money.transfer.storage;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.hamcrest.number.BigDecimalCloseTo.closeTo;

import java.math.BigDecimal;

import org.bitbucket.mif.money.transfer.core.domain.AccountId;
import org.bitbucket.mif.money.transfer.core.domain.exceptions.ClientNotFoundException;
import org.junit.Test;

public class ClientStorageTest {

	@Test
	public void createClientTest() {
		//GIVEN
		ClientStorage clientStorage = new ClientStorage();
		AccountId accountId = new AccountId("acc1");
		//WHEN
		clientStorage.createAccountWith(accountId);
		//THEN
		assertThat(clientStorage.retrieveClientBy(accountId), notNullValue());
	}

	@Test(expected = ClientNotFoundException.class)
	public void deleteAccountWith_ifClientNotExist_throwsException() {
		//GIVEN
		ClientStorage clientStorage = new ClientStorage();
		AccountId accountId = new AccountId("acc1");
		//WHEN
		clientStorage.deleteAccountWith(accountId);
		//THEN
		assertThat(clientStorage.retrieveClientBy(accountId), nullValue());
	}

	@Test
	public void deleteAccountWith_ifClientExist_noException() {
		//GIVEN
		ClientStorage clientStorage = new ClientStorage();
		AccountId accountId = new AccountId("acc1");
		clientStorage.createAccountWith(accountId);
		//WHEN
		clientStorage.deleteAccountWith(accountId);
	}

	@Test(expected = ClientNotFoundException.class)
	public void getBalanceOf_ifClientNotExist_throwsException() {
		//GIVEN
		ClientStorage clientStorage = new ClientStorage();
		AccountId accountId = new AccountId("acc1");
		//WHEN
		clientStorage.getBalanceOf(accountId);
		//THEN
		assertThat(clientStorage.retrieveClientBy(accountId), nullValue());
	}

	@Test
	public void getBalanceOf_ifClientExist_noException() {
		//GIVEN
		ClientStorage clientStorage = new ClientStorage();
		AccountId accountId = new AccountId("acc1");
		clientStorage.createAccountWith(accountId);
		//WHEN
		BigDecimal result = clientStorage.getBalanceOf(accountId);
		//THEN
		assertThat(result, closeTo(BigDecimal.ZERO, BigDecimal.ZERO));
	}

	@Test(expected = ClientNotFoundException.class)
	public void updateCreditLimitFor_ifClientNotExist_throwsException() {
		//GIVEN
		ClientStorage clientStorage = new ClientStorage();
		AccountId accountId = new AccountId("acc1");
		//WHEN
		clientStorage.updateCreditLimitFor(accountId, BigDecimal.ONE);
		//THEN
		assertThat(clientStorage.retrieveClientBy(accountId), nullValue());
	}

	@Test
	public void updateCreditLimitFor_ifClientExist_noException() {
		//GIVEN
		ClientStorage clientStorage = new ClientStorage();
		AccountId accountId = new AccountId("acc1");
		clientStorage.createAccountWith(accountId);
		//WHEN
		clientStorage.updateCreditLimitFor(accountId, BigDecimal.ONE);
	}

	@Test(expected = ClientNotFoundException.class)
	public void retrieveClientBy_ifClientNotExist_throwsException() {
		//GIVEN
		ClientStorage clientStorage = new ClientStorage();
		AccountId accountId = new AccountId("acc1");
		//WHEN
		clientStorage.retrieveClientBy(accountId);
		//THEN
		assertThat(clientStorage.retrieveClientBy(accountId), nullValue());
	}

}
