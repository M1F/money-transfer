package org.bitbucket.mif.money.transfer.core.domain;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicReference;

class ClientBalance {
	private final AtomicReference<BigDecimal> balance;

	ClientBalance() {
		balance = new AtomicReference<>(BigDecimal.ZERO);
	}

	BigDecimal get() {
		return balance.get();
	}

	void add(BigDecimal amount) {
		if (amount.signum() < 0) {
			throw new IllegalArgumentException("You try to add negative amount, use subtract method");
		}
		balance.updateAndGet(balanceValue -> balanceValue.add(amount));
	}

	void subtract(BigDecimal amount) {
		BigDecimal amountToSubtract = amount.abs();

		balance.updateAndGet(balanceValue -> balanceValue.subtract(amountToSubtract));
	}

}
