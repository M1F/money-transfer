package org.bitbucket.mif.money.transfer.core.cash.movement;

import org.bitbucket.mif.money.transfer.core.domain.AccountId;
import org.bitbucket.mif.money.transfer.core.domain.Client;

public interface ClientProvider {

	Client retrieveClientBy(AccountId accountId);
}
