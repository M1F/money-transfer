package org.bitbucket.mif.money.transfer.core.account.management;

import java.math.BigDecimal;

import org.bitbucket.mif.money.transfer.core.account.BalanceProvider;
import org.bitbucket.mif.money.transfer.core.domain.AccountId;

public class AccountManagement {
	private final AccountInfoStorage accountInfoStorage;
	private final BalanceProvider balanceProvider;

	public AccountManagement(AccountInfoStorage accountInfoStorage, BalanceProvider balanceProvider) {
		this.accountInfoStorage = accountInfoStorage;
		this.balanceProvider = balanceProvider;
	}

	public void createAccountWith(AccountId accountId) {
		accountInfoStorage.createAccountWith(accountId);
	}

	public void deleteAccountWith(AccountId accountId) {
		accountInfoStorage.deleteAccountWith(accountId);
	}

	public void updateCreditLimitFor(AccountId accountId, BigDecimal newCreditLimit) {
		accountInfoStorage.updateCreditLimitFor(accountId, newCreditLimit);
	}

	public BigDecimal getBalanceOf(AccountId accountId) {
		return balanceProvider.getBalanceOf(accountId);
	}
}
