package org.bitbucket.mif.money.transfer.core.account;

import java.math.BigDecimal;

import org.bitbucket.mif.money.transfer.core.domain.AccountId;

public interface BalanceProvider {
	BigDecimal getBalanceOf(AccountId accountId);
}
