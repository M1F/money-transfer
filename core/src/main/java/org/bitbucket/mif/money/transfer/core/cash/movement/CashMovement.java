package org.bitbucket.mif.money.transfer.core.cash.movement;

import java.math.BigDecimal;

import org.bitbucket.mif.money.transfer.core.domain.AccountId;
import org.bitbucket.mif.money.transfer.core.domain.CashTransfer;
import org.bitbucket.mif.money.transfer.core.domain.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CashMovement {
	private static final Logger LOGGER = LoggerFactory.getLogger(CashMovement.class);
	private final ClientProvider clientProvider;

	public CashMovement(ClientProvider clientProvider) {
		this.clientProvider = clientProvider;
	}

	public void depositFor(AccountId accountId, BigDecimal depositeAmount) {
		Client client = clientProvider.retrieveClientBy(accountId);
		client.deposit(depositeAmount);
	}

	public void transferOf(CashTransfer cashTransfer) {
		AccountId accountFrom = cashTransfer.accountFrom();
		Client clientFrom = clientProvider.retrieveClientBy(accountFrom);
		AccountId accountTo = cashTransfer.accountTo();
		Client clientTo = clientProvider.retrieveClientBy(accountTo);
		//don't change order
		LOGGER.info("Transfer from {} to {} started", accountFrom, accountTo);
		clientFrom.withdrawal(cashTransfer.amount());
		LOGGER.info("Withdrawal from {} was done", accountFrom);
		clientTo.deposit(cashTransfer.amount());
		LOGGER.info("Deposit to {} was done", accountTo);

	}
}
