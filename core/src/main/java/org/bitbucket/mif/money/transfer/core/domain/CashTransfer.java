package org.bitbucket.mif.money.transfer.core.domain;

import java.math.BigDecimal;

public class CashTransfer {
	private final AccountId from;
	private final AccountId to;
	private final BigDecimal transferAmount;

	public CashTransfer(AccountId from, AccountId to, BigDecimal transferAmount) {
		this.from = from;
		this.to = to;
		this.transferAmount = transferAmount;
	}

	public AccountId accountFrom() {
		return from;
	}

	public AccountId accountTo() {
		return to;
	}

	public BigDecimal amount() {
		return transferAmount;
	}
}
