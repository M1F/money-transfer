package org.bitbucket.mif.money.transfer.storage;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.bitbucket.mif.money.transfer.core.account.BalanceProvider;
import org.bitbucket.mif.money.transfer.core.account.management.AccountInfoStorage;
import org.bitbucket.mif.money.transfer.core.cash.movement.ClientProvider;
import org.bitbucket.mif.money.transfer.core.domain.AccountId;
import org.bitbucket.mif.money.transfer.core.domain.Client;
import org.bitbucket.mif.money.transfer.core.domain.exceptions.ClientNotFoundException;

public class ClientStorage implements AccountInfoStorage, BalanceProvider, ClientProvider {
	private final Map<AccountId, Client> clients;

	public ClientStorage() {
		clients = new ConcurrentHashMap<>();
	}

	@Override
	public BigDecimal getBalanceOf(AccountId accountId) {
		return Optional.ofNullable(clients.get(accountId))
				.map(Client::balance).orElseThrow(() -> new ClientNotFoundException("Client with id " + accountId + "not found"));
	}

	@Override
	public void createAccountWith(AccountId accountId) {
		clients.computeIfAbsent(accountId, Client::new);
	}

	@Override
	public void deleteAccountWith(AccountId accountId) {
		if (clients.remove(accountId) == null) {
			throw new ClientNotFoundException("Client with id " + accountId + "doesn't exist");
		}
	}

	@Override
	public void updateCreditLimitFor(AccountId accountId, BigDecimal newCreditLimit) {
		Optional.ofNullable(clients.get(accountId)).ifPresentOrElse(client -> client.updateCreditLimit(newCreditLimit),
				() -> {
					throw new ClientNotFoundException("Client with id " + accountId + "not found");
				});

	}

	@Override
	public Client retrieveClientBy(AccountId accountId) {
		return Optional.ofNullable(clients.get(accountId))
				.orElseThrow(() -> new ClientNotFoundException("Client with id " + accountId + "not found"));
	}
}
