package org.bitbucket.mif.money.transfer.core.account.management;

import java.math.BigDecimal;

import org.bitbucket.mif.money.transfer.core.domain.AccountId;

public interface AccountInfoStorage {
	void createAccountWith(AccountId accountId);

	void deleteAccountWith(AccountId accountId);

	void updateCreditLimitFor(AccountId accountId, BigDecimal newCreditLimit);

}
