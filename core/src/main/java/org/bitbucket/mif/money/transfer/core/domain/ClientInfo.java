package org.bitbucket.mif.money.transfer.core.domain;

import java.math.BigDecimal;

class ClientInfo {
	private final BigDecimal creditLimit;

	ClientInfo() {
		this(BigDecimal.ZERO);
	}

	private ClientInfo(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	ClientInfo updateCreditLimit(BigDecimal newCreditLimit) {
		return new ClientInfo(newCreditLimit);
	}

	boolean isAboveLimit(BigDecimal amount) {
		return amount.compareTo(creditLimit) > 0;
	}
}
