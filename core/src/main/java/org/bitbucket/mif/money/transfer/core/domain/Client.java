package org.bitbucket.mif.money.transfer.core.domain;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicReference;

import org.bitbucket.mif.money.transfer.core.domain.exceptions.NotEnoughMoneyException;

public class Client {
	private final AccountId accountId;
	private final ClientBalance clientBalance;
	private final AtomicReference<ClientInfo> clientInfo;

	public Client(AccountId accountId) {
		this.accountId = accountId;
		this.clientBalance = new ClientBalance();
		this.clientInfo = new AtomicReference<>(new ClientInfo());
	}

	public void withdrawal(BigDecimal withdrawalAmount) {
		synchronized (clientBalance) {
			BigDecimal creditAmount = withdrawalAmount.subtract(clientBalance.get());
			if (clientInfo.get().isAboveLimit(creditAmount)) {
				throw new NotEnoughMoneyException("You can't do withdrawal, you don't have enough money");
			}
			clientBalance.subtract(withdrawalAmount);
		}
	}

	public void deposit(BigDecimal depositAmount) {
		clientBalance.add(depositAmount);
	}

	public BigDecimal balance() {
		return clientBalance.get();
	}

	public void updateCreditLimit(BigDecimal newCreditLimit) {
		clientInfo.updateAndGet(clientInfo -> clientInfo.updateCreditLimit(newCreditLimit));
	}
}
