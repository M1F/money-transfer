package org.bitbucket.mif.money.transfer.core.domain.exceptions;

public class NotEnoughMoneyException extends RuntimeException {
	public NotEnoughMoneyException(String message) {
		super(message);
	}
}
