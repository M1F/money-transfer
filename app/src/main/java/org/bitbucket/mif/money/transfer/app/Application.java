package org.bitbucket.mif.money.transfer.app;

import java.io.IOException;

import org.bitbucket.mif.money.transfer.app.config.ServerPropertiesProvider;
import org.bitbucket.mif.money.transfer.server.HttpServer;
import org.bitbucket.mif.money.transfer.server.HttpServerFactory;

public class Application {
	private static HttpServer httpServer;

	public static void main(String[] args) throws IOException {
		//load props
		ServerPropertiesProvider serverPropertiesProvider = new ServerPropertiesProvider();
		serverPropertiesProvider.loadProperties();
		int serverPort = serverPropertiesProvider.getServerPort();
		//start server
		httpServer = HttpServerFactory.create(serverPort);
		httpServer.start();
	}

	public static void start() throws IOException {
		main(null);
	}

	public static void stop() {
		httpServer.stop();
	}
}
