package org.bitbucket.mif.money.transfer.app.config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for retrieve some config from property file
 */
public class ServerPropertiesProvider {
	private static final Logger LOGGER = LoggerFactory.getLogger(ServerPropertiesProvider.class);
	private static final String PROPERTIES_FILE_NAME = "serverConfig.properties";
	private static final String PORT_PROPERTY_NAME = "server.port";
	private final Properties serverProperties;

	public ServerPropertiesProvider() {
		serverProperties = new Properties();
	}

	/**
	 * invoke this method in order to read information from property file
	 */
	public void loadProperties() throws IOException {
		try (InputStream input = getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME)) {
			serverProperties.load(input);
		}
		catch (FileNotFoundException e) {
			LOGGER.error("Can't find the file: {} for start load server configs", PROPERTIES_FILE_NAME, e);
			throw e;
		}
		catch (IOException e) {
			LOGGER.error("Exception during loading file {} ", PROPERTIES_FILE_NAME, e);
			throw e;
		}
	}

	public int getServerPort() {
		try {
			String port = serverProperties.getProperty(PORT_PROPERTY_NAME);
			return Integer.parseInt(port);
		}
		catch (NumberFormatException e) {
			LOGGER.error("property 'port' should be a number", e);
			throw e;
		}
	}

}
