package org.bitbucket.mif.money.transfer.e2e.tests;

import java.io.IOException;
import java.math.BigDecimal;

class HttpRequestSender {

	static void createAccountWith(String accountId) throws IOException {
		String createAccountMethod = "POST";
		String body = "{\"accountId\":\"" + accountId + "\"}";
		try (AutoCloseableHttpURLConnection connection = new AutoCloseableHttpURLConnection(URLBuilder.getCreateAccountUrl(),
				createAccountMethod)) {
			connection.init();
			connection.writeJsonBody(body);
			connection.readResponse();
		}
	}

	static void updateCreditLimitFor(String accountId, BigDecimal limit) throws IOException {
		String url = URLBuilder.buildUpdateCreditLimitURLFor(accountId);
		String method = "PUT";
		String body = "{\"newValue\":\"" + limit + "\"}";
		//WHEN
		try (AutoCloseableHttpURLConnection connection = new AutoCloseableHttpURLConnection(url, method)) {
			connection.init();
			connection.writeJsonBody(body);
			connection.readResponse();
		}
	}

	static void makeDepositFor(String accountId, BigDecimal amount) throws IOException {
		String url = URLBuilder.buildDepositURLFor(accountId);
		String method = "POST";
		String body = "{\"amount\":\"" + amount + "\"}";
		try (AutoCloseableHttpURLConnection connection = new AutoCloseableHttpURLConnection(url, method)) {
			connection.init();
			connection.writeJsonBody(body);
			connection.readResponse();
		}
	}

	public static String getAccountBalance(String accountId) throws Exception {
		String getBalanceURL = URLBuilder.buildGetBalanceURLFor(accountId);
		String getBalanceMethod = "GET";
		//WHEN
		try (AutoCloseableHttpURLConnection connection = new AutoCloseableHttpURLConnection(getBalanceURL, getBalanceMethod)) {
			connection.init();
			return connection.readResponse();
		}

	}
}
