package org.bitbucket.mif.money.transfer.e2e.tests;

import static org.bitbucket.mif.money.transfer.e2e.tests.HttpRequestSender.createAccountWith;
import static org.bitbucket.mif.money.transfer.e2e.tests.HttpRequestSender.getAccountBalance;
import static org.bitbucket.mif.money.transfer.e2e.tests.HttpRequestSender.makeDepositFor;
import static org.bitbucket.mif.money.transfer.e2e.tests.HttpRequestSender.updateCreditLimitFor;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import java.io.IOException;
import java.math.BigDecimal;

import org.bitbucket.mif.money.transfer.app.Application;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class AppTest {

	@BeforeClass
	public static void startApplication() throws IOException {
		Application.start();
	}

	@AfterClass
	public static void stopApplication() {
		Application.stop();
	}

	@Test
	public void createAccountTest() throws Exception {
		//GIVEN
		String method = "POST";
		String body = "{\"accountId\":\"acc1\"}";
		//WHEN
		String result;
		try (AutoCloseableHttpURLConnection connection = new AutoCloseableHttpURLConnection(URLBuilder.getCreateAccountUrl(), method)) {
			connection.init();
			connection.writeJsonBody(body);
			result = connection.readResponse();
		}
		//THEN
		String expected = "created";
		assertThat(result, is(expected));
	}

	@Test
	public void updateCreditLimitAccountTest() throws Exception {
		//GIVEN
		String accountId = "account0";
		BigDecimal limit = BigDecimal.valueOf(1.1);
		createAccountWith(accountId);
		String url = URLBuilder.buildUpdateCreditLimitURLFor(accountId);
		String method = "PUT";
		String body = "{\"newValue\":\"" + limit + "\"}";
		//WHEN
		String result;
		try (AutoCloseableHttpURLConnection connection = new AutoCloseableHttpURLConnection(url, method)) {
			connection.init();
			connection.writeJsonBody(body);
			result = connection.readResponse();
		}
		//THEN
		String expected = "updated";
		assertThat(result, is(expected));
	}

	@Test
	public void deleteAccountTest() throws Exception {
		//GIVEN
		String accountId = "accountForRemoving";
		createAccountWith(accountId);
		String url = URLBuilder.buildDeleteAccountURLFor(accountId);
		String method = "DELETE";
		//WHEN
		String result;
		try (AutoCloseableHttpURLConnection connection = new AutoCloseableHttpURLConnection(url, method)) {
			connection.init();
			result = connection.readResponse();
		}
		//THEN
		String expected = "deleted";
		assertThat(result, is(expected));
	}

	@Test
	public void getAccountBalanceOfNewAccountTest() throws Exception {
		//GIVEN
		String accountId = "account1";
		createAccountWith(accountId);
		String getBalanceURL = URLBuilder.buildGetBalanceURLFor(accountId);
		String getBalanceMethod = "GET";
		//WHEN
		String result;
		try (AutoCloseableHttpURLConnection connection = new AutoCloseableHttpURLConnection(getBalanceURL, getBalanceMethod)) {
			connection.init();
			result = connection.readResponse();
		}
		//THEN
		String expected = BigDecimal.ZERO.toString();
		assertThat(result, is(expected));
	}

	@Test
	public void depositTest() throws Exception {
		//GIVEN
		String accountId = "account2";
		BigDecimal amount = BigDecimal.valueOf(1000);
		createAccountWith(accountId);
		String url = URLBuilder.buildDepositURLFor(accountId);
		String method = "POST";
		String body = "{\"amount\":\"" + amount + "\"}";
		//WHEN
		String result;
		try (AutoCloseableHttpURLConnection connection = new AutoCloseableHttpURLConnection(url, method)) {
			connection.init();
			connection.writeJsonBody(body);
			result = connection.readResponse();
		}
		//THEN
		String expected = "deposit was successful";
		String balanceResult = getAccountBalance(accountId);

		assertThat(result, is(expected));
		assertThat(balanceResult, is(amount.toString()));
	}

	@Test
	public void transferTest() throws Exception {
		//GIVEN
		String accountIdFrom = "account3";
		String accountIdTo = "account5";
		BigDecimal amountToTransfer = BigDecimal.valueOf(2000);
		createAccountWith(accountIdFrom);
		createAccountWith(accountIdTo);
		updateCreditLimitFor(accountIdFrom, BigDecimal.valueOf(1500));
		makeDepositFor(accountIdFrom, BigDecimal.valueOf(500));

		String method = "POST";
		String body =
				"{\"accountFrom\":\"" + accountIdFrom + "\",\"accountTo\":\"" + accountIdTo + "\",\"amount\":\"" + amountToTransfer + "\"}";
		//WHEN
		String result;
		try (AutoCloseableHttpURLConnection connection = new AutoCloseableHttpURLConnection(URLBuilder.getTransferUrl(), method)) {
			connection.init();
			connection.writeJsonBody(body);
			result = connection.readResponse();
		}
		//THEN
		String expected = "cash movement was done successfully";

		assertThat(result, is(expected));
		String balanceResultAccountFrom = getAccountBalance(accountIdFrom);
		String balanceResultAccountTo = getAccountBalance(accountIdTo);

		assertThat(result, is(expected));
		assertThat(balanceResultAccountFrom, is(BigDecimal.valueOf(-1500).toString()));
		assertThat(balanceResultAccountTo, is(amountToTransfer.toString()));
	}

}

