package org.bitbucket.mif.money.transfer.e2e.tests;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

class AutoCloseableHttpURLConnection implements AutoCloseable {
	private final HttpURLConnection connection;
	private final String method;

	AutoCloseableHttpURLConnection(String url, String method) throws IOException {
		this.method = method;
		URL myurl = new URL(url);
		connection = (HttpURLConnection) myurl.openConnection();

	}

	void init() throws ProtocolException {
		connection.setConnectTimeout(5000);
		connection.setReadTimeout(5000);
		connection.setRequestMethod(method);
	}

	String readResponse() throws IOException {
		try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {

			String line;
			StringBuilder content = new StringBuilder();

			while ((line = in.readLine()) != null) {
				content.append(line);
			}
			return content.toString();
		}
	}

	void writeJsonBody(String json) throws IOException {
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setDoOutput(true);
		try (OutputStream os = connection.getOutputStream()) {
			os.write(json.getBytes());
			os.flush();
		}
	}

	@Override
	public void close() {
		connection.disconnect();
	}

}
