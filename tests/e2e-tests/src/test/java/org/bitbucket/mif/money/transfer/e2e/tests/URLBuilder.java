package org.bitbucket.mif.money.transfer.e2e.tests;

class URLBuilder {

	static String buildUpdateCreditLimitURLFor(String accountId) {
		return "http://localhost:8000/money-transfer-api-v1/account/" + accountId + "/credit-limit";
	}

	static String buildGetBalanceURLFor(String accountId) {
		return "http://localhost:8000/money-transfer-api-v1/account/" + accountId + "/balance";
	}

	static String buildDeleteAccountURLFor(String accountId) {
		return "http://localhost:8000/money-transfer-api-v1/account/" + accountId;
	}

	static String buildDepositURLFor(String accountId) {
		return "http://localhost:8000/money-transfer-api-v1/cash-movement/deposit/" + accountId;
	}

	static String getCreateAccountUrl() {
		return "http://localhost:8000/money-transfer-api-v1/account";
	}

	static String getTransferUrl() {
		return "http://localhost:8000/money-transfer-api-v1/cash-movement/transfer";
	}
}
