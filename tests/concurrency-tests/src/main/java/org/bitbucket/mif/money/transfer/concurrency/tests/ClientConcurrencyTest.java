package org.bitbucket.mif.money.transfer.concurrency.tests;

import static org.openjdk.jcstress.annotations.Expect.ACCEPTABLE;
import static org.openjdk.jcstress.annotations.Expect.FORBIDDEN;

import java.math.BigDecimal;

import org.bitbucket.mif.money.transfer.core.domain.AccountId;
import org.bitbucket.mif.money.transfer.core.domain.Client;
import org.bitbucket.mif.money.transfer.core.domain.exceptions.NotEnoughMoneyException;
import org.openjdk.jcstress.annotations.Actor;
import org.openjdk.jcstress.annotations.Arbiter;
import org.openjdk.jcstress.annotations.Description;
import org.openjdk.jcstress.annotations.JCStressTest;
import org.openjdk.jcstress.annotations.Outcome;
import org.openjdk.jcstress.annotations.State;
import org.openjdk.jcstress.infra.results.I_Result;

public class ClientConcurrencyTest {

	@State
	public static class ClientState {
		Client client = new Client(new AccountId("1"));

		{
			client.updateCreditLimit(BigDecimal.valueOf(100_000));
		}
	}

	@JCStressTest
	@Description("Test client deposit")
	@Outcome(id = "9000", expect = ACCEPTABLE, desc = "client deposit is thread safe")
	@Outcome(expect = FORBIDDEN, desc = "concurrency issue in core logic!.")
	public static class DepositTest {
		@Actor
		public void actor1(ClientState state) {
			state.client.deposit(BigDecimal.valueOf(1000));
			state.client.deposit(BigDecimal.valueOf(1000));
			state.client.deposit(BigDecimal.valueOf(1000));
		}

		@Actor
		public void actor2(ClientState state) {
			state.client.deposit(BigDecimal.valueOf(1000));
			state.client.deposit(BigDecimal.valueOf(1000));
			state.client.deposit(BigDecimal.valueOf(1000));
		}

		@Actor
		public void actor3(ClientState state) {
			state.client.deposit(BigDecimal.valueOf(1000));
			state.client.deposit(BigDecimal.valueOf(1000));
			state.client.deposit(BigDecimal.valueOf(1000));
		}

		@Arbiter
		public void arbiter(ClientState state, I_Result result) {
			result.r1 = state.client.balance().intValue();
		}
	}

	@JCStressTest
	@Description("Test client withdrawal")
	@Outcome(id = "-9000", expect = ACCEPTABLE, desc = "client withdrawal is thread safe")
	@Outcome(expect = FORBIDDEN, desc = "concurrency issue in core logic!.")
	public static class WithdrawalTest {
		@Actor
		public void actor1(ClientState state) {
			state.client.withdrawal(BigDecimal.valueOf(1000));
			state.client.withdrawal(BigDecimal.valueOf(1000));
			state.client.withdrawal(BigDecimal.valueOf(1000));
		}

		@Actor
		public void actor2(ClientState state) {
			state.client.withdrawal(BigDecimal.valueOf(1000));
			state.client.withdrawal(BigDecimal.valueOf(1000));
			state.client.withdrawal(BigDecimal.valueOf(1000));
		}

		@Actor
		public void actor3(ClientState state) {
			state.client.withdrawal(BigDecimal.valueOf(1000));
			state.client.withdrawal(BigDecimal.valueOf(1000));
			state.client.withdrawal(BigDecimal.valueOf(1000));
		}

		@Arbiter
		public void arbiter(ClientState state, I_Result result) {
			result.r1 = state.client.balance().intValue();
		}
	}

	@JCStressTest
	@Description("Test client deposit and withdrawal")
	@Outcome(id = "-1000", expect = ACCEPTABLE, desc = "client deposit and withdrawal is thread safe")
	@Outcome(expect = FORBIDDEN, desc = "concurrency issue in core logic!.")
	public static class DepositeWithdrawalTest {
		@Actor
		public void actor1(ClientState state) {
			state.client.withdrawal(BigDecimal.valueOf(1000));
			state.client.deposit(BigDecimal.valueOf(1000));
			state.client.withdrawal(BigDecimal.valueOf(1000));
		}

		@Actor
		public void actor2(ClientState state) {
			state.client.deposit(BigDecimal.valueOf(1000));
			state.client.deposit(BigDecimal.valueOf(1000));
			state.client.withdrawal(BigDecimal.valueOf(1000));
		}

		@Actor
		public void actor3(ClientState state) {
			state.client.withdrawal(BigDecimal.valueOf(1000));
			state.client.withdrawal(BigDecimal.valueOf(1000));
			state.client.deposit(BigDecimal.valueOf(1000));
		}

		@Arbiter
		public void arbiter(ClientState state, I_Result result) {
			result.r1 = state.client.balance().intValue();
		}
	}

	@JCStressTest
	@Description("Test client deposit and withdrawal with some limitation issues")
	@Outcome(id = "-100000", expect = ACCEPTABLE, desc = "client deposit and withdrawal is thread safe with limit check")
	@Outcome(id = "0", expect = ACCEPTABLE, desc = "first withdrawal in actor3 was executed before all actor2")
	@Outcome(id = "100000", expect = ACCEPTABLE, desc = "both withdrawals in actor3 was executed before all actor2")
	@Outcome(expect = FORBIDDEN, desc = "concurrency issue in core logic!.")
	public static class DepositeWithdrawalLimitTest {
		@Actor
		public void actor1(ClientState state) {
			try {
				state.client.withdrawal(BigDecimal.valueOf(100_000));
				state.client.deposit(BigDecimal.valueOf(100_000));
				state.client.withdrawal(BigDecimal.valueOf(100_000));
			}
			catch (NotEnoughMoneyException e) {
				//skip
			}

		}

		@Actor
		public void actor2(ClientState state) {
			try {
				state.client.deposit(BigDecimal.valueOf(100_000));
				state.client.deposit(BigDecimal.valueOf(100_000));
				state.client.withdrawal(BigDecimal.valueOf(100_000));
			}
			catch (NotEnoughMoneyException e) {
				//skip
			}
		}

		@Actor
		public void actor3(ClientState state) {
			try {

				state.client.withdrawal(BigDecimal.valueOf(100_000));
				state.client.withdrawal(BigDecimal.valueOf(100_000));
				state.client.deposit(BigDecimal.valueOf(100_000));
			}
			catch (NotEnoughMoneyException e) {
				//skip
			}

		}

		@Arbiter
		public void arbiter(ClientState state, I_Result result) {
			result.r1 = state.client.balance().intValue();
		}
	}
}
