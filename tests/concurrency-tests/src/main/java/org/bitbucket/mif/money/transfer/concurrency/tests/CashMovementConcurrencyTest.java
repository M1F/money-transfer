package org.bitbucket.mif.money.transfer.concurrency.tests;

import static org.openjdk.jcstress.annotations.Expect.ACCEPTABLE;
import static org.openjdk.jcstress.annotations.Expect.FORBIDDEN;

import java.math.BigDecimal;

import org.bitbucket.mif.money.transfer.core.cash.movement.CashMovement;
import org.bitbucket.mif.money.transfer.core.domain.AccountId;
import org.bitbucket.mif.money.transfer.core.domain.CashTransfer;
import org.bitbucket.mif.money.transfer.core.domain.exceptions.NotEnoughMoneyException;
import org.bitbucket.mif.money.transfer.storage.ClientStorage;
import org.openjdk.jcstress.annotations.Actor;
import org.openjdk.jcstress.annotations.Arbiter;
import org.openjdk.jcstress.annotations.Description;
import org.openjdk.jcstress.annotations.JCStressTest;
import org.openjdk.jcstress.annotations.Outcome;
import org.openjdk.jcstress.annotations.State;
import org.openjdk.jcstress.infra.results.II_Result;

public class CashMovementConcurrencyTest {
	@State
	public static class CashMovementState {
		ClientStorage clientStorage = new ClientStorage();
		CashMovement cashMovement = new CashMovement(clientStorage);

		{
			clientStorage.createAccountWith(new AccountId("1"));
			clientStorage.createAccountWith(new AccountId("2"));
			clientStorage.updateCreditLimitFor(new AccountId("1"), BigDecimal.valueOf(1000));
			clientStorage.updateCreditLimitFor(new AccountId("2"), BigDecimal.valueOf(1000));
		}
	}

	@JCStressTest
	@Description("Test cash movement transfer, always enough money")
	@Outcome(id = "-100, 100", expect = ACCEPTABLE, desc = "cashMovement.transferOf is thread safe")
	@Outcome(expect = FORBIDDEN, desc = "concurrency issue in core logic for transfer money!")
	public static class TransferMoneyTest {
		@Actor
		public void actor1(CashMovementState state) {
			state.cashMovement.transferOf(new CashTransfer(new AccountId("1"), new AccountId("2"), BigDecimal.valueOf(100)));
		}

		@Actor
		public void actor2(CashMovementState state) {
			state.cashMovement.transferOf(new CashTransfer(new AccountId("1"), new AccountId("2"), BigDecimal.valueOf(100)));
		}

		@Actor
		public void actor3(CashMovementState state) {
			state.cashMovement.transferOf(new CashTransfer(new AccountId("2"), new AccountId("1"), BigDecimal.valueOf(100)));
		}

		@Arbiter
		public void arbiter(CashMovementState state, II_Result result) {
			result.r1 = state.clientStorage.getBalanceOf(new AccountId("1")).intValue();
			result.r2 = state.clientStorage.getBalanceOf(new AccountId("2")).intValue();
		}
	}

	@JCStressTest
	@Description("Test cash movement transfer, sometimes not enough money")
	@Outcome(id = "-1000, 1000", expect = ACCEPTABLE, desc = "actor3 is first, then it got not enough money exception, and acctor1 or 2 same")
	@Outcome(id = "-500, 500", expect = ACCEPTABLE, desc = "actor3 second, and then others")
	@Outcome(id = "500, -500", expect = ACCEPTABLE, desc = "actor3 is last, then actor2/1 will get not enough money exception")
	@Outcome(expect = FORBIDDEN, desc = "concurrency issue in core logic for transfer money!")
	public static class TransferMoneyWithLimitTest {

		@Actor
		public void actor1(CashMovementState state) {
			try {
				state.cashMovement.transferOf(new CashTransfer(new AccountId("1"), new AccountId("2"), BigDecimal.valueOf(1000)));
			}
			catch (NotEnoughMoneyException e) {
				//ignore
			}
		}

		@Actor
		public void actor2(CashMovementState state) {
			try {
				state.cashMovement.transferOf(new CashTransfer(new AccountId("1"), new AccountId("2"), BigDecimal.valueOf(1000)));
			}
			catch (NotEnoughMoneyException e) {
				//ignore
			}
		}

		@Actor
		public void actor3(CashMovementState state) {
			try {
				state.cashMovement.transferOf(new CashTransfer(new AccountId("2"), new AccountId("1"), BigDecimal.valueOf(1500)));
			}
			catch (NotEnoughMoneyException e) {
				//ignore
			}
		}

		@Arbiter
		public void arbiter(CashMovementState state, II_Result result) {
			result.r1 = state.clientStorage.getBalanceOf(new AccountId("1")).intValue();
			result.r2 = state.clientStorage.getBalanceOf(new AccountId("2")).intValue();
		}
	}

	@JCStressTest
	@Description("Test cash movement transfer, only one should pass")
	@Outcome(id = "-1000, 1000", expect = ACCEPTABLE, desc = "cashMovement.transferOf is thread safe")
	@Outcome(expect = FORBIDDEN, desc = "concurrency issue in core logic for transfer money!")
	public static class TransferMoneyOnlyOneTest {
		@Actor
		public void actor1(CashMovementState state) {
			try {
				state.cashMovement.transferOf(new CashTransfer(new AccountId("1"), new AccountId("2"), BigDecimal.valueOf(1000)));
			}
			catch (NotEnoughMoneyException e) {
				//ignore
			}

		}

		@Actor
		public void actor2(CashMovementState state) {
			try {
				state.cashMovement.transferOf(new CashTransfer(new AccountId("1"), new AccountId("2"), BigDecimal.valueOf(1000)));
			}
			catch (NotEnoughMoneyException e) {
				//ignore
			}
		}

		@Actor
		public void actor3(CashMovementState state) {
			try {
				state.cashMovement.transferOf(new CashTransfer(new AccountId("1"), new AccountId("2"), BigDecimal.valueOf(1000)));
			}
			catch (NotEnoughMoneyException e) {
				//ignore
			}
		}

		@Arbiter
		public void arbiter(CashMovementState state, II_Result result) {
			result.r1 = state.clientStorage.getBalanceOf(new AccountId("1")).intValue();
			result.r2 = state.clientStorage.getBalanceOf(new AccountId("2")).intValue();
		}
	}

}
