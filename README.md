# money-transfer
The main purposes of this application - transfer money from one account to another.

In order to run concurrency test run: java - jar yourLocalPathTo\money-transfer\tests\concurrency-tests\target\money-transfer-tests.jar

E2E test runs during build.

Here you can see hhtp url in order to call application ("acc1", "acc2" - it's a accountId, could be different): 

create Account: curl -X POST localhost:8000/money-transfer-api-v1/account -d {\"accountId\":\"acc1\"}
update Account: curl -X PUT localhost:8000/money-transfer-api-v1/account/acc2/credit-limit -d {\"newValue\":\"1000\"}
delete Account: curl -X DELETE localhost:8000/money-transfer-api-v1/account/acc3
retrieve balance Account: curl -X GET localhost:8000/money-transfer-api-v1/account/acc4/balance
transfer money: curl -X POST localhost:8000/money-transfer-api-v1/cash-movement/transfer -d {\"accountFrom\":\"acc1\",\"accountTo\":\"acc2\",\"amount\":\"1000\"}
transfer money: curl -X POST localhost:8000/money-transfer-api-v1/cash-movement/deposit/acc5 -d {\"amount\":\"1000\"}