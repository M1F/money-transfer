package org.bitbucket.mif.money.transfer.api.rest.cash.movement;

import java.math.BigDecimal;
import java.util.function.Function;

import org.bitbucket.mif.money.transfer.api.CashMovementResource;
import org.bitbucket.mif.money.transfer.api.rest.RestApi;

public class CashMovementDepositApi<T> implements RestApi<T> {
	private static final String PATH = APPLICATION_PATH + "/cash-movement/deposit/";

	private final CashMovementResource cashMovementResource;
	private final Function<T, String> accountIdConverter;
	private final Function<T, BigDecimal> depositAmountConverter;

	public CashMovementDepositApi(CashMovementResource cashMovementResource,
			Function<T, String> accountIdConverter,
			Function<T, BigDecimal> depositAmountConverter) {

		this.cashMovementResource = cashMovementResource;
		this.accountIdConverter = accountIdConverter;
		this.depositAmountConverter = depositAmountConverter;
	}

	@Override
	public String getPath() {
		return PATH;
	}

	@Override
	public String getMethod() {
		return "POST";
	}

	@Override
	public String process(T request) {
		String accountId = accountIdConverter.apply(request);
		BigDecimal depositAmount = depositAmountConverter.apply(request);
		cashMovementResource.deposit(accountId, depositAmount);
		return "deposit was successful";
	}
}
