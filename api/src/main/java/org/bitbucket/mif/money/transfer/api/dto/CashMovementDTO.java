package org.bitbucket.mif.money.transfer.api.dto;

import java.math.BigDecimal;

public class CashMovementDTO {
	private String accountFrom;
	private String accountTo;
	private BigDecimal amount;

	public CashMovementDTO(String accountFrom, String accountTo, BigDecimal amount) {
		this.accountFrom = accountFrom;
		this.accountTo = accountTo;
		this.amount = amount;
	}

	public CashMovementDTO() {
	}

	public String getAccountFrom() {
		return accountFrom;
	}

	public void setAccountFrom(String accountFrom) {
		this.accountFrom = accountFrom;
	}

	public String getAccountTo() {
		return accountTo;
	}

	public void setAccountTo(String accountTo) {
		this.accountTo = accountTo;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "CashMovementDTO{" +
				"accountFrom='" + accountFrom + '\'' +
				", accountTo='" + accountTo + '\'' +
				", amount=" + amount +
				'}';
	}
}
