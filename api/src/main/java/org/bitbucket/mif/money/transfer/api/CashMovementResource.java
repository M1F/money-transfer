package org.bitbucket.mif.money.transfer.api;

import java.math.BigDecimal;

import org.bitbucket.mif.money.transfer.api.dto.CashMovementDTO;

public interface CashMovementResource {

	void deposit(String account, BigDecimal depositAmount);

	void transfer(CashMovementDTO transaction);
}
