package org.bitbucket.mif.money.transfer.api.rest.cash.movement;

import java.util.function.Function;

import org.bitbucket.mif.money.transfer.api.CashMovementResource;
import org.bitbucket.mif.money.transfer.api.dto.CashMovementDTO;
import org.bitbucket.mif.money.transfer.api.rest.RestApi;

public class CashMovementTransferApi<T> implements RestApi<T> {
	private static final String PATH = APPLICATION_PATH + "/cash-movement/transfer";

	private final CashMovementResource cashMovementResource;
	private final Function<T, CashMovementDTO> cashMovementConverter;

	public CashMovementTransferApi(CashMovementResource cashMovementResource, Function<T, CashMovementDTO> cashMovementConverter) {
		this.cashMovementResource = cashMovementResource;
		this.cashMovementConverter = cashMovementConverter;
	}

	@Override
	public String getPath() {
		return PATH;
	}

	@Override
	public String getMethod() {
		return "POST";
	}

	@Override
	public String process(T request) {
		CashMovementDTO cashMovementDTO = cashMovementConverter.apply(request);
		cashMovementResource.transfer(cashMovementDTO);
		return "cash movement was done successfully";
	}
}
