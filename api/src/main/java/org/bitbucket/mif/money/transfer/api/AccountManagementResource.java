package org.bitbucket.mif.money.transfer.api;

import java.math.BigDecimal;

public interface AccountManagementResource {

	void createAccount(String accountId);

	void setCreditLimit(String accountId, BigDecimal creditLimit);

	void deleteAccount(String accountId);

	BigDecimal getBalance(String accountId);

}
