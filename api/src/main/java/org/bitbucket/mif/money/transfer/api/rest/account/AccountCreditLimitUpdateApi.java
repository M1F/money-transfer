package org.bitbucket.mif.money.transfer.api.rest.account;

import java.math.BigDecimal;
import java.util.function.Function;

import org.bitbucket.mif.money.transfer.api.AccountManagementResource;
import org.bitbucket.mif.money.transfer.api.rest.RestApi;

public class AccountCreditLimitUpdateApi<T> implements RestApi<T> {
	private static final String PATH = APPLICATION_PATH + "/account/";

	private final AccountManagementResource accountManagementResource;
	private final Function<T, String> accountIdConverter;
	private final Function<T, BigDecimal> creditLimitConverter;

	public AccountCreditLimitUpdateApi(AccountManagementResource accountManagementResource, Function<T, String> accountIdConverter,
			Function<T, BigDecimal> creditLimitConverter) {
		this.accountManagementResource = accountManagementResource;
		this.accountIdConverter = accountIdConverter;
		this.creditLimitConverter = creditLimitConverter;
	}

	@Override
	public String getPath() {
		return PATH;
	}

	@Override
	public String getMethod() {
		return "PUT";
	}

	@Override
	public String process(T request) {
		String accountId = accountIdConverter.apply(request);
		BigDecimal creditLimit = creditLimitConverter.apply(request);
		accountManagementResource.setCreditLimit(accountId, creditLimit);
		return "updated";
	}

}
