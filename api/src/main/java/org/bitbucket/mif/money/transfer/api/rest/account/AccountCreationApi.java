package org.bitbucket.mif.money.transfer.api.rest.account;

import java.util.function.Function;

import org.bitbucket.mif.money.transfer.api.AccountManagementResource;
import org.bitbucket.mif.money.transfer.api.rest.RestApi;

public class AccountCreationApi<T> implements RestApi<T> {

	private static final String PATH = APPLICATION_PATH + "/account";

	private final AccountManagementResource accountManagementResource;
	private final Function<T, String> accountIdConverter;

	public AccountCreationApi(AccountManagementResource accountManagementResource, Function<T, String> accountIdConverter) {
		this.accountManagementResource = accountManagementResource;
		this.accountIdConverter = accountIdConverter;
	}

	@Override
	public String getPath() {
		return PATH;
	}

	@Override
	public String getMethod() {
		return "POST";
	}

	@Override
	public String process(T request) {
		String accountId = accountIdConverter.apply(request);
		accountManagementResource.createAccount(accountId);
		return "created";
	}
}
