package org.bitbucket.mif.money.transfer.api.rest;

public interface RestApi<T> {
	String APPLICATION_PATH = "/money-transfer-api-v1";

	default String getPath() {
		return APPLICATION_PATH;
	}

	String getMethod();

	String process(T request);

}
